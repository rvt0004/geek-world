//
//  ViewController.m
//  demoApp
//
//  Created by DeftDeskSol on 05/10/18.
//  Copyright © 2018 DeftDeskSol. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"
#import "ViewController3.h"
#import "ViewController4.h"
@import Firebase;



@interface ViewController ()
{
    NSDictionary *dict1;
     NSDictionary *dict2;
    
    NSString *city;
    NSString *name;
    NSString *phone;
    NSMutableArray *array1;
    
}
@property (strong, nonatomic) FIRDatabaseReference *ref;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self lableView];
    [self loadData];
    [self loadTableView];
    [self getData];
    
//    self.content = @[@"red",@"blue",@"green"];
    
}

-(void) lableView {
    _label=[[UILabel alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-100)/2, 80, 100, 50)];//Set frame of label in your viewcontroller.
    [_label setBackgroundColor:[UIColor lightGrayColor]];//Set background color of label.
    //    [_label sizeToFit];
    [_label setText:@"Welcome"];//Set text in label.
    [_label setTextColor:[UIColor blackColor]];//Set text color in label.
    [_label setTextAlignment:NSTextAlignmentCenter];//Set text alignment in label.
    [_label setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];//Set line adjustment.
    [_label setLineBreakMode:NSLineBreakByCharWrapping];//Set linebreaking mode..
    [_label setNumberOfLines:1];//Set number of lines in label.
    //    [_label.layer setCornerRadius:25.0];//Set corner radius of label to change the shape.
    [_label.layer setBorderWidth:2.0f];//Set border width of label.
    //    [_label setClipsToBounds:YES];//Set its to YES for Corner radius to work.
    [_label.layer setBorderColor:[UIColor blackColor].CGColor];//Set Border color.
    [self.view addSubview:_label];//Add it to the view of your choice.
}

-(void) getData{
    NSString *strUrl = [NSString stringWithFormat:@"https://demoapp-1539150109026.firebaseio.com/login"];
    FIRDatabaseReference *ref = [[FIRDatabase database] referenceFromURL:strUrl];
    [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *post = snapshot.value;
        NSLog(@"%@",post);
        dict1 = post;
        dict2 = dict1[@"zoey"];
        city = dict2[@"city"];
        name = dict2[@"name "];
        phone = dict2[@"phoneNumber"];
        array1 = [[NSMutableArray alloc] init];
        [array1 addObject:city];
         [array1 addObject:name];
         [array1 addObject:phone];
       
        [self.table reloadData];
    
        
    }];
    
}

-(void) postData{
    NSString *strUrl = [NSString stringWithFormat:@"https://demoapp-1539150109026.firebaseio.com/login"];
    [FIRAuth auth];
    FIRDatabaseReference *ref = [[FIRDatabase database] referenceFromURL:strUrl];
    [ref observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
       
        
           }];
}
    

-(void) loadData{
    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-320)/2,180,320,150)];
    newView.backgroundColor=[UIColor blueColor];
    UITextView *mytext = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, 320-20, 150-20)];
    mytext.backgroundColor = [UIColor yellowColor];
    mytext.textColor = [UIColor blackColor];
    mytext.editable = YES;
    mytext.font = [UIFont systemFontOfSize:17];
    mytext.text = @"Mytext";
    mytext.scrollEnabled=YES;
    [newView addSubview:mytext];
    [self.view addSubview:newView];
    self.view.backgroundColor = [UIColor lightGrayColor];
    
}
- (void) loadTableView{
   self.table = [[UITableView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-320)/2,340,320,200)];
    self.table.delegate = self;
    self.table.dataSource = self;
    [self.view addSubview:self.table];
    }

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array1.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [self.table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    cell.textLabel.text =  [NSString stringWithFormat:@"%@",[array1 objectAtIndex:indexPath.row]];
    
//    [array1 objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //NSLog(@"title of cell %@", [_content objectAtIndex:indexPath.row]);
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 0){
        self.viewMoved = true;
        ViewController2* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController2"];
        controller.viewMoved = true;

        [self.navigationController pushViewController:controller animated:YES];


    }
    else  if (indexPath.row == 1){
        self.viewMoved = false;
        ViewController2* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController2"];
        controller.viewMoved = false;

        [self.navigationController pushViewController:controller animated:YES];


    }
    else {
        ViewController4* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController4"];


        [self.navigationController pushViewController:controller animated:YES];
    }


    for (id object in cell.superview.subviews) {
        if ([object isKindOfClass:[UITableViewCell class]]) {
            UITableViewCell *cellNotSelected = (UITableViewCell*)object;
            cellNotSelected.textLabel.textColor = [UIColor blackColor];
        }
    }

    cell.textLabel.textColor = [UIColor redColor];
//    if (indexPath == 0){
//        ViewController2* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController2"];
//
//
//        [self.navigationController pushViewController:controller animated:YES];
//
//
//    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

  }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
