//
//  AppDelegate.h
//  demoApp
//
//  Created by DeftDeskSol on 05/10/18.
//  Copyright © 2018 DeftDeskSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

