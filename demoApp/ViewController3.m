//
//  ViewController3.m
//  demoApp
//
//  Created by DeftDeskSol on 06/10/18.
//  Copyright © 2018 DeftDeskSol. All rights reserved.
//

#import "ViewController3.h"

@interface ViewController3 ()
@property NSArray *imageArray;

@end

@implementation ViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor whiteColor]];
    self.imageArray = @[@"rafting.jpg",@"Nice-Cool-Image.jpg",@"rafting.jpg",@"Nice-Cool-Image.jpg",@"rafting.jpg",@"Nice-Cool-Image.jpg",@"rafting.jpg",@"Nice-Cool-Image.jpg",@"rafting.jpg",@"Nice-Cool-Image.jpg"];
    [self.view addSubview:_collectionView];
    // Do any additional setup after loading the view.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    UIImageView* imageVw =  [[UIImageView alloc] initWithFrame:CGRectMake(0, 0 , 50, 50)];
    self.imageVw.backgroundColor = [UIColor blackColor];
     imageVw.image = [UIImage imageNamed:[_imageArray objectAtIndex:indexPath.row]];
    [cell.contentView addSubview:imageVw];
   cell.backgroundColor=[UIColor orangeColor];
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(expandImage:)];
    tapped.numberOfTapsRequired = 1;
    [imageVw setUserInteractionEnabled:YES];
    imageVw.tag = indexPath.row;
    [imageVw addGestureRecognizer:tapped];
    
    [cell.contentView addSubview:imageVw];
    
    
    
    
    
    
    return cell;
}

-(void)expandImage:(UITapGestureRecognizer*)recogniser{
UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 50, self.view.frame.size.width , self.view.frame.size.height)];
view1.backgroundColor = [UIColor greenColor];
[self.view addSubview:view1];

UIButton *closeButton = [[UIButton alloc]initWithFrame:CGRectMake(200, 10, 50, 40)];
[closeButton setTitle:@"Close" forState:UIControlStateNormal];
[closeButton addTarget:self action:@selector(Closetab) forControlEvents:UIControlEventTouchUpInside];
   
[view1 addSubview:closeButton];
{
    
    view1.hidden = NO;
    
    UIImageView *photoView1 = (UIImageView*)recogniser.view;
    photoView1.frame = CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height);
    photoView1.accessibilityIdentifier = @"rafting.jpg";
    
    
    //photoView.clipsToBounds = YES;
    
    // photoView.accessibilityIdentifier = @"apple1.png";
    
    photoView1.contentMode =  UIViewContentModeScaleAspectFill;
    
    photoView1.clipsToBounds = YES;
    
    [view1 addSubview:photoView1];
}
}

-(void)Closetab:(UITapGestureRecognizer*)recogniser
{
    
    
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 50);
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 5, 0, 5); // top, left, bottom, right
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
