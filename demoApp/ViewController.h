//
//  ViewController.h
//  demoApp
//
//  Created by DeftDeskSol on 05/10/18.
//  Copyright © 2018 DeftDeskSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UILabel *label;

@property (strong,nonatomic) UITableView *table;
@property (strong,nonatomic) NSArray     *content;
@property BOOL viewMoved;

@end

