//
//  main.m
//  demoApp
//
//  Created by DeftDeskSol on 05/10/18.
//  Copyright © 2018 DeftDeskSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
