//
//  ViewController2.h
//  demoApp
//
//  Created by DeftDeskSol on 06/10/18.
//  Copyright © 2018 DeftDeskSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController2 : UIViewController
@property UIImageView *myImage;
@property UIView *view2;
@property BOOL viewMoved;

@end
